package com.clevertec.task2.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.clevertec.task2.data.Repository
import com.clevertec.task2.databinding.FragmentListBinding
import com.clevertec.task2.ui.ItemViewAdapter

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class ItemListFragment : Fragment() {

    private var _binding: FragmentListBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val items = Repository().getAllItems()
        val navigateFun: (Int) -> Unit = { itemId ->
                ItemListFragmentDirections.actionItemListToItemDetails("", idx = itemId)
                        .also { findNavController().navigate(it) } }
        binding.recycler.apply {
            setHasFixedSize(true)
            this.layoutManager = LinearLayoutManager(context)
            adapter = ItemViewAdapter(items, navigateFun)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}