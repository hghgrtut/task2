package com.clevertec.task2.ui.fragments

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.clevertec.task2.R
import com.clevertec.task2.data.Repository
import com.clevertec.task2.databinding.FragmentDetailsBinding
import com.clevertec.task2.ui.extensions.findImageButton
import com.clevertec.task2.ui.extensions.setPlaceholderBackground

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class ItemDetailsFragment : Fragment() {

    private var _binding: FragmentDetailsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) sharedElementEnterTransition =
            TransitionInflater.from(requireContext()).inflateTransition(android.R.transition.move)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val item = Repository().getItem(navArgs<ItemDetailsFragmentArgs>().value.idx)
        binding.title.text = item.title
        binding.description.text = item.description
        binding.pic.setPlaceholderBackground()
        binding.toolbar.run {
            findImageButton(R.id.back).setOnClickListener {
                findNavController().navigate(R.id.action_ItemDetails_to_ItemList) }
            findImageButton(R.id.close).setOnClickListener {
                val exitDialog = ExitDialog()
                val manager = parentFragmentManager
                exitDialog.show(manager, "myDialog")
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}