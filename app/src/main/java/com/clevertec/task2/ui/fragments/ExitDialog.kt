package com.clevertec.task2.ui.fragments

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.clevertec.task2.R
import kotlin.system.exitProcess

class ExitDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setTitle(R.string.exit_from_app)
                .setMessage(R.string.are_you_sure_want_to_exit)
                .setCancelable(true)
                .setPositiveButton(R.string.yes) { dialog, id ->
                    exitProcess(0)
                }
                .setNegativeButton(R.string.no) { dialog, id ->
                    onCancel(dialog)
                }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}