package com.clevertec.task2.ui.extensions

import android.view.View
import android.widget.ImageButton
import androidx.annotation.IdRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.res.ResourcesCompat
import com.clevertec.task2.R

fun AppCompatImageView.setPlaceholderBackground() {
    background = ResourcesCompat.getDrawable(resources, R.drawable.shape, context.theme)
}

fun View.findImageButton(@IdRes id: Int) = findViewById<ImageButton>(id)