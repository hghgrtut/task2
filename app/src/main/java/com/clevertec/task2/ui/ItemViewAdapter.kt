package com.clevertec.task2.ui

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.drawable.BitmapDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.findFragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.clevertec.task2.R
import com.clevertec.task2.data.Item
import com.clevertec.task2.ui.extensions.setPlaceholderBackground
import com.clevertec.task2.ui.fragments.ItemListFragment
import com.clevertec.task2.ui.fragments.ItemListFragmentDirections

/**
 * Should be only used inside ItemListFragment because it places listener necessary for this
 * fragment in ViewHolder. If you want to use it with different fragment you should delete assertion
 * from onCreateViewHolder() and replace some logic in onBindViewHolder()
 */
class ItemViewAdapter(private val items: Array<Item>, private val navigateFun: (Int) -> Unit) :
    RecyclerView.Adapter<ItemViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        assert(parent.findFragment<ItemListFragment>() is ItemListFragment)
parent.findFragment<ItemListFragment>().findNavController()
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.title.text = item.title
        holder.description.text = item.description
        holder.itemView.setOnClickListener { navigateFun(item.id) }
        holder.pic.setPlaceholderBackground()
    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val title = view.findViewById<TextView>(R.id.title)
        val description = view.findViewById<TextView>(R.id.description)
        val pic = view.findViewById<AppCompatImageView>(R.id.pic)
    }
}