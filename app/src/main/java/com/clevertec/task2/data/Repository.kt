package com.clevertec.task2.data

class Repository {
    fun getAllItems(): Array<Item> = Array(1000) { Item(it + 1) }

    fun getItem(id: Int): Item = Item(id)
}