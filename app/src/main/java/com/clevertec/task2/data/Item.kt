package com.clevertec.task2.data

data class Item(val id: Int, val title: String = "Title$id", val description: String = "Description $id")